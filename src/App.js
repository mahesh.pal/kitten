import React, { Component } from 'react';
import { useSelector} from 'react-redux'
import {
  Routes,
  Route,
  useLocation,
  Navigate,
} from "react-router-dom";
import Layout from './components/Layout';
import Register from './components/Register';







class App extends Component {


  render() {
    return (
      <div style={{margin:0}}>
        <Routes>
          <Route path='/register' element={
            <Register />

          } />
          <Route
            path="/"
            element={
              <RequireAuth redirectTo="/register">
                <Layout />
              </RequireAuth>
            }
          />
        </Routes>
      </div>
    );
  }
}

export default App;



function RequireAuth({ children, redirectTo }) {
  const user=useSelector(state=>state.user)
  let location = useLocation();
  if (!user.name) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to={redirectTo} state={{ from: location }} replace />;
  }

  return children;
}