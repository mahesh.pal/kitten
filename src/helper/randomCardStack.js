


export default  function deckofCards(){
    let cards = []
    let totalCards=0
    const cardObject = {
        1: 'CAT_CARD',
        2: 'DEFUSE_CARD',
        3: 'SUFFLE_CARD',
        4: 'EXPLODING_KITTEN_CARD'
    };
  
    while (totalCards!==5) {
        let random = Math.floor(Math.random()*4 + 1);
        if (cardObject[random]) {
            cards.push(cardObject[random])
            totalCards++
        }
    }

    console.log(cards);
  
    return cards
  }
  