let initialState = { cards:[] ,defusedCards:0,flipedCard:'',gameStatus:''};

let gameReducer= (state = initialState, action) => {
  switch (action.type) {
    case 'START_GAME':
      return {cards:action.payload,defusedCards:0,flipedCard:'',gameStatus:''};
    case 'REMOVE_CARD':
      let totalCards=[...state.cards]
      totalCards.pop()
      return { ...state,cards:totalCards,flipedCard:action.payload };
    case 'DEFUSE_CARD':
      return { ...state,defusedCards:action.payload };
    case 'GAME_LOST':
      return { ...state,gameStatus: action.payload };
    case 'GAME_WON':
      return { ...state,gameStatus: action.payload};
    default:
      return state;
  }
};

export default gameReducer;


