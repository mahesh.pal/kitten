

let initialState = { name: null,wins:0 }

let userReducer = (state = initialState, action) => {
  switch (action.type) {
    case "REGISTER":
      return { ...state, name: action.payload };
      case "USER_WON":
        return { ...state, wins:state.wins+1 };
    default:
      return state;
  }
};

export default userReducer;
