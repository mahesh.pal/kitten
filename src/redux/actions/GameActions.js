
import deckofCards from '../../helper/randomCardStack'






export const startTheGame = () => {
  return { type: 'START_GAME', payload: deckofCards() };
};


const removeCard = (card) => {
  return { type: 'REMOVE_CARD', payload: card };
};




const defuseCard = (totalDefuseCards) => {
  return { type: 'DEFUSE_CARD', payload: totalDefuseCards };
};

const gameLost = () => {
  return { type: 'GAME_LOST', payload: 'You lost the game' }
}
const gameWon = () => {
  return { type: 'GAME_WON', payload: 'You WON the game' }
};
export const checkGameStatus = () => (dispatch, getState) => {
  const { cards} = getState().game;
  if (cards.length === 0) {
    dispatch(gameWon())
    dispatch({ type: 'USER_WON' })
  }
};


export const flipACard = (flipedCard) => (dispatch, getState) => {
  const {defusedCards } = getState().game;
  if (flipedCard === 'CAT_CARD') {
    dispatch(removeCard(flipedCard))
}

  else if (flipedCard === 'DEFUSE_CARD') {
    dispatch(removeCard(flipedCard));
    dispatch(defuseCard(defusedCards + 1))

  }
  else if (flipedCard === 'SUFFLE_CARD') {
    dispatch(removeCard(flipedCard));
    setTimeout(() => dispatch(startTheGame()), 500);

  }
  else if (flipedCard === 'EXPLODING_KITTEN_CARD') {
    if (defusedCards > 0) {
      dispatch(removeCard(flipedCard))
      dispatch(defuseCard(defusedCards - 1));

    }

    else {
      dispatch(gameLost());
      dispatch(removeCard(flipedCard))
    }
  }

  flipedCard!=='SUFFLE_CARD' && dispatch(checkGameStatus())
};



