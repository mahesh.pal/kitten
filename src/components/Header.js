import React from 'react';
import {makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { useSelector } from 'react-redux';


const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  title: {
   marginRight:100
  },
  toolbar:{
    display:'flex',

  }
 
}));

export default function PrimarySearchAppBar() {
  const classes = useStyles();
  const {defusedCards}=useSelector(state=>state.game)
  const {name,wins}=useSelector(state=>state.user)
  return (
 
      <AppBar  position="static">
        <Toolbar className={classes.toolbar}>
          <Typography className={classes.title} variant="h6">
            Defuse Cards :{defusedCards}
          </Typography>
          <Typography className={classes.title} variant="h6" >
            Name :{name}
          </Typography>
          <Typography className={classes.title} variant="h6">
            Wins :{wins}
          </Typography>

        </Toolbar>
      </AppBar>
    
  );
}
