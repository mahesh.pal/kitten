import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import DeckCard from './Card'
import Box from '@material-ui/core/Box';
import DeckCards from './DeckCards';
import { Button } from '@material-ui/core';
import { startTheGame } from '../redux/actions/GameActions'
import Result from './Result';


export default function Game() {
  let game = useSelector(state => state.game)
  let dispatch = useDispatch()
  const [showResult, setShowResult] = useState(false)

  useEffect(() => {
    game.gameStatus?setShowResult(true):setShowResult(false)
  }, [game.gameStatus])


// useEffect(() => {
//   subscriber.subscribe("user-notify")

//   return () => {
    
//   }
// }, [])



// subscriber.on("message", (channel, message) => {
//   console.log("Received data :" + message)
// })





  const startGame = () => {
    dispatch(startTheGame())
  }




  return (
    <React.Fragment>
      <Box
        display="flex"
      >
        <Result
          onConfirm={() => {
            setShowResult(false)
            startGame()
          }}
          open={showResult}
          status={game.gameStatus}
        />
        <Box display='flex' bgcolor="lightgreen" flex={3} justifyContent='space-around' alignItems='center' >
          <Button onClick={startGame} variant='contained'>Start </Button>
          <DeckCards />
          <DeckCard name={game.flipedCard} />

        </Box>
        <Box display='flex' bgcolor="lightblue" flex={1}>
          leader board
        </Box>
      </Box>
    </React.Fragment>
  )
}
