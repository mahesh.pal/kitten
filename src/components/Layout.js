import React from 'react'
import Game from './Game';
import Header from './Header'



export default function Layout() {
    return (
        <React.Fragment>
            <Header/>
            <Game />
        </React.Fragment>
    )
}
