import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { useSelector, useDispatch } from 'react-redux';
import { flipACard } from '../redux/actions/GameActions';

const useStyles = makeStyles({
    root: {
        width: 345,
        height: 440,
    },
    media: {
        height: 140,
    },
});

export default function DeckCards(props) {
    const classes = useStyles();
    const dispatch = useDispatch()
    const game = useSelector((state) => state.game)


    const openedCard = () => {

        let card = [...game.cards].pop()
        dispatch(flipACard(card))
    }


    return (
        <Card onClick={openedCard} className={classes.root}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image="/static/images/cards/contemplative-reptile.jpg"
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        Remaing Cards: {game.cards.length}
                    </Typography>
                   
                </CardContent>
            </CardActionArea>
        </Card>
    );
}
