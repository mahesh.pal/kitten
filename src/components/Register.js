import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  useNavigate,
  useLocation,
} from "react-router-dom";
import { Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField'
import Box from '@material-ui/core/Box';



export default function Register() {
  let navigate = useNavigate();
  let location = useLocation();
  const dispatch = useDispatch()
  const [name, setName] = useState(null)
  let from = location.state.from.pathname || "/";

  function handleSubmit(event) {
    event.preventDefault();
    dispatch({ type: 'REGISTER', payload: name })
    // auth.signin(username, () => {
    // Send them back to the page they tried to visit when they were
    // redirected to the login page. Use { replace: true } so we don't create
    // another entry in the history stack for the login page.  This means that
    // when they get to the protected page and click the back button, they
    // won't end up back on the login page, which is also really nice for the
    // user experience.
    navigate(from, { replace: true });
    // });
  }

  // return (
 
  //     <Grid container
  //       direction="row"
  //       justifyContent="center"
  //       alignItems="center"
  //       spacing={3}>
  //       <Grid item xs={6}>
  //         <form onSubmit={handleSubmit} autoComplete="off">
  //           <TextField onChange={(e) => setName(e.target.value)} required size="small" id="outlined-basic" label="Username" variant="outlined" />
  //           <Button type="submit" variant="contained" color="primary">Start</Button>
  //         </form>
  //       </Grid>

  //     </Grid>
  
  // )

  return (
    <React.Fragment>
      <Box 
        display="flex" 
        height={'100vh'} 
        bgcolor="lightgreen"
        alignItems="center"
        justifyContent="center"
      >
      <form onSubmit={handleSubmit} autoComplete="off">
            <TextField onChange={(e) => setName(e.target.value)} required size="small" id="outlined-basic" label="Username" variant="outlined" />
            <Button type="submit" variant="contained" color="primary">Start</Button>
          </form>
      </Box>
  
    </React.Fragment>
  )


}